<img src="https://res.cloudinary.com/practicaldev/image/fetch/s--78sV_n0e--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/un1vp3qoyd13gznq1pvt.png" width="100%" height="350px" style="object-fit: cover;">

![](https://gitlab.com/majksa/frontend-template/badges/master/pipeline.svg)
![](https://img.shields.io/badge/git-Git%20Lab-orange)
![](https://img.shields.io/badge/api-GraphQL-da0093)

---

## Goal

Main goal is to provide best prepared frontend in Svelte for a GraphQL API with user registration and login.

Focused on:

- typescript
- `sveltejs/*` packages
- GraphQL connection via `urql/*`
- codestyle checking via **Prettier** and `ninjify/*`
- static analysing via **ESLint**
- static svelte analysis via **Svelte Check**

# Summary

1. [Initial Configuration](#initial-configuration)
2. [Installation](#installation)
3. [Quality Assurance](#quality-assurance)
4. [Connecting to Backend](#connecting-to-backend)
5. [Usage](#usage)
6. [Changelog](#changelog)

# Initial Configuration

Since it is all running inside docker containers, no additional configuration is needed! Though you need to install some
tools first:

- [docker](https://docs.docker.com/get-docker/) and docker compose (included in docker from version 3.4)

## Optional:

- **make** - _for easier scripts configuration_
- **node** and **npm** - _if you want to run application without having to start docker containers_

# Installation

1. Create a local copy of _docker-compose.local.dist.yml_
   - `cp docker-compose.local.dist.yml docker-compose.local.yml`
2. Start the server and run database migrations:
   1. [Using Makefile](#using-makefile)
   2. [Using docker compose](#using-docker-compose)
3. And the application is up and running on url: http://localhost:3000

If you wish to change the port, you can do so in _docker-compose.local.yml_: `services > server > ports`

## Using Makefile

If you have installed Make, you can follow the steps bellow for the easiest setup.

`make compose-dev` - starts docker network

## Using docker compose

`docker compose -f docker-compose.local.yml up -d --force-recreate --build --remove-orphans` - starts docker network

# Quality Assurance

To make sure our code quality, we use 3 different tools:

- **Prettier**: for formatting
- **ESLint**: for static analysis
- **Svelte Check**: for static svelte analysis

You can simply run all of them using `make fix-and-qa`.

**To make sure these tools run before every commit, please run: `git config core.hooksPath .hooks`**

# Connecting to Backend

Do not forget that this is just a frontend for a backend project. Just run it and specify the url
in: `services > app > environment > VITE_API_URL`.

# Usage

## Executing request

To execute a request safely, please make use of the Request component. \
What it does is it only displays the content inside it, if request has finished successfully. \
Otherwise, it calls the error handlers (more on them down bellow).

The Request component has 3 props:

- store: the operation store that will contain the result
- call: the query | mutation | subscription function
- handlers: array of error handlers, by default contains the loggingHandler and securityHandler

A very simple example that displays the current user username.

```sveltehtml

<script lang='ts'>
  import Request from '$components/Request.svelte';
  import { operationStore, query, gql } from '@urql/svelte';

  const user = operationStore(gql`
    query me {
      Me {
        username
      }
    }
  `);
</script>

<Request store={user} call={query}>
  You are logged in as {$user.data.Me.username}!!
</Request>
```

### Error handlers

These are special objects with the purpose to handle GraphQL errors.

- filter: return true if the error handler should be used to the given error
- handle: run any javascript with the error, return true if handler should render the component
- component: create a component of the given type and pass the error as the `error` prop

#### Examples:

- Log all error messages to console and do not display any component.

```ts
import { ErrorHandler } from '$lib/graphql';

const echoMessageHandler: ErrorHandler = {
  filter: () => true,
  handle: (error) => {
    console.log(error.message);
    return false;
  },
  component: null,
};
```

- Display the error message as Text component

```ts
import Text from './Text.svelte';

const printMessageHandler: ErrorHandler = {
  filter: () => true,
  handle: () => true,
  component: Text,
};
```

Text.svelte

```sveltehtml

<script lang='ts'>
  import { GraphQLError } from 'graphql';

  export let error: GraphQLError;
</script>

{error.message}
```

## Localization

We use [i18n](https://github.com/kaisermann/svelte-i18n/blob/main/docs/Getting%20Started.md) for localization.

Localization files are located in /src/lang folder. All json files from there are added to the database.

Instead of writing text directly to the webpage, save it in the json dictionary for future translations.

## Loading screen

If you want to load something before showing the content to the world, use the loading store.

Before you start the loading process, ask for a new loader: `const finishLoading = addStaticLoader();`.
When the process is finished, just call the received loader: `finishLoading();`. And boom, that's everything.

# Changelog

## 1.2.0 | 21.4.2020

- moved dictionary files from /public/lang to /src/lang and started including them automatically using VITE
- all pages require being logged in from now on, except for the ones specified in /src/lib/stores.ts#exposedRoutes (using regex)
- automatically import /src/global.scss in all svelte files
- migrated from deployment using static pages to nodejs
- added $src route alias to access /src directly from js (without `../../` mess)
- tweaked docker-compose.yml and removed docker-compose.local.dist.yml (update your local copy to prevent errors)
- added loading screen
