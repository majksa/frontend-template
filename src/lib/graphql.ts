import type { GraphQLError } from 'graphql';
import type { SvelteComponent } from 'svelte';
import { goto } from '$app/navigation';

export interface Component {
  type: SvelteComponent;
  data: object;
}

export interface ErrorHandler {
  filter: (error: GraphQLError) => boolean;
  // True if handler is supposed to render the component
  handle: (error: GraphQLError) => boolean;
  component: SvelteComponent | null;
}

export const loggingHandler: ErrorHandler = {
  filter: () => true,
  handle: (error) => {
    console.error(error);
    return false;
  },
  component: null,
};

export const securityHandler: ErrorHandler = {
  filter: (error) => error.extensions.category === 'security',
  handle: () => {
    goto('/');
    return false;
  },
  component: null,
};

export function defaultHandlers(...custom: ErrorHandler[]) {
  return [loggingHandler, securityHandler, ...custom];
}

export function upload(token: string, file: File) {
  const data = new FormData();
  data.append('token', token);
  data.append('file', file);

  let apiUrl = import.meta.env.VITE_API_URL;
  apiUrl = typeof apiUrl === 'boolean' ? '' : apiUrl;
  apiUrl.replace('/graphql', '/upload');

  return fetch(apiUrl, {
    method: 'POST',
    body: data,
  });
}
