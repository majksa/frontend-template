import { goto } from '$app/navigation';
import { writable } from 'svelte/store';

export const url = import.meta.env.VITE_API_URL;

/**
 * Loading
 */
export const loadingStore = writable(0);

export const addStaticLoader: () => () => void = () => {
  loadingStore.update((n) => n + 1);
  return () => loadingStore.update((n) => n - 1);
};

/**
 * Exposed routes
 */
const exposedRoutes: Array<RegExp> = [/^\/login/];

export const isExposed = (path: string) => {
  for (const i in exposedRoutes) {
    if (exposedRoutes[i].test(path)) {
      return true;
    }
  }

  return false;
};

/**
 * Backlinks
 */
const backlink = new Array<string>();

export const addBacklink = (link: string) => backlink.push(link);

export const saveBacklink = () => addBacklink(window.location.href);

export const hasBacklink = () => backlink.length !== 0;

export const peekBacklink = () => backlink[backlink.length - 1];

export const gotoBacklink = () => {
  if (!hasBacklink()) {
    return;
  }
  return goto(backlink.pop());
};
