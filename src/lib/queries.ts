import { gql } from '@urql/svelte';
import type { TypedDocumentNode } from '@graphql-typed-document-node/core/src/index';

export const login: TypedDocumentNode = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      username
      email
    }
  }
`;
