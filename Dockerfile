FROM node:17

RUN apt-get update
RUN apt-get -y install apache2
RUN a2enmod proxy_html
COPY apache.conf /etc/apache2/app.conf
RUN cat /etc/apache2/app.conf >> /etc/apache2/apache2.conf

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn --frozen-lockfile
COPY . ./

ARG API_URL
ENV VITE_API_URL=$API_URL
RUN yarn run build

CMD /bin/bash -c "service apache2 start; node build"
EXPOSE 80