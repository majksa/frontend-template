qa:
	yarn run lint

fix-and-qa:
	yarn run format
	yarn run lint

#####################
# LOCAL DEVELOPMENT #
#####################
compose-dev: compose-dev-stop
	docker compose -f docker-compose.local.yml up -d --force-recreate --build --remove-orphans
	docker logs --follow $(shell basename $(CURDIR))-app-1

compose-dev-stop:
	NETTE_DEBUG=1 docker compose -f docker-compose.local.yml down --remove-orphans
