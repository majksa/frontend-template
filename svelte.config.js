import adapter from '@sveltejs/adapter-node';
import preprocess from 'svelte-preprocess';
import path from 'path';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess({
    scss: {
      includePaths: ['src'],
      prependData: `@import 'src/global';`,
    },
    sourceMap: true,
  }),

  kit: {
    adapter: adapter({
      out: 'build',
    }),

    vite: {
      resolve: {
        alias: {
          $src: path.resolve('./src'),
          $components: path.resolve('./src/components'),
          $tools: path.resolve('./src/lib/tools'),
        },
      },
      optimizeDeps: {
        exclude: ['@urql/svelte'],
      },
    },
  },
};

export default config;
